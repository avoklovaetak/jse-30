package ru.volkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.repository.IProjectRepository;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.exception.empty.EmptyNameException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;

import java.util.Optional;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Optional<Project> add(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(project);
    }

}
