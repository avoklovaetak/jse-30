package ru.volkova.tm.command.bonds;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.util.TerminalUtil;

public class TaskBindByProjectIdCommand extends AbstractProjectTaskClass {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "task bind to project by project id";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getProjectTaskService()
                .bindTaskByProjectId(userId, projectId, taskId);
    }

    @NotNull
    @Override
    public String name() {
        return "task-bind-by-project-id";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
