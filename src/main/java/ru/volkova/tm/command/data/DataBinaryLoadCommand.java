package ru.volkova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.dto.Domain;
import java.io.*;

public class DataBinaryLoadCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Nullable
    @Override
    public String description() {
        return "load data binary";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[LOAD DATA BINARY]");
        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
        if (serviceLocator == null) return;
        serviceLocator.getAuthService().logout();
    }

    @Nullable
    @Override
    public String name() {
        return "data-load-binary";
    }

}
