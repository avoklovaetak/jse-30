package ru.volkova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataJsonLoadJaxbCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Nullable
    @Override
    public String description() {
        return "load json data from JAXB";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[JSON DATA JAXB LOAD]");
        System.setProperty(SYSTEM_JSON_PROPERTY_NAME, SYSTEM_JSON_PROPERTY_VALUE);
        @NotNull final File file = new File(FILE_JAXB_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(JAXB_JSON_PROPERTY_NAME, JAXB_JSON_PROPERTY_VALUE);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);

        setDomain(domain);
    }

    @Nullable
    @Override
    public String name() {
        return "data-load-json-jaxb";
    }


}
