package ru.volkova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataYamlLoadFasterxmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Nullable
    @Override
    public String description() {
        return "load yaml data";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[LOAD YAML DATA]");
        @NotNull final String yaml = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_YAML)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        @NotNull final Domain domain = objectMapper.readValue(yaml, Domain.class);
        setDomain(domain);
    }

    @Nullable
    @Override
    public String name() {
        return "data-load-yaml";
    }


}
