package ru.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectStartByNameCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "start project by name";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[START PROJECT]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull final Optional<Project> project = serviceLocator.getProjectService().startOneByName(userId, name);
        if (!project.isPresent()) throw new ProjectNotFoundException();
    }

    @NotNull
    @Override
    public String name() {
        return "project-start-by-name";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
