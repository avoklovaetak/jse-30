package ru.volkova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.entity.User;

import java.util.List;
import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    void clear();

    @NotNull
    List<User> findAll();

    @NotNull
    Optional<User> findByEmail(@NotNull String email);

    @NotNull
    Optional<User> findById(@NotNull String id);

    @NotNull
    Optional<User> findByLogin(@NotNull String login);

    void lockByEmail(@NotNull String email);

    void lockById(@NotNull String id);

    void lockByLogin(@NotNull String login);

    void removeByEmail(@NotNull String email);

    void removeById(@NotNull String id);

    void removeByLogin(@NotNull String login);

    void unlockByEmail(@NotNull String email);

    void unlockById(@NotNull String id);

    void unlockByLogin(@NotNull String login);

}
